package main

import (
	"fmt"
	"io"
	"net"
	"net/http"
)

func hello(w http.ResponseWriter, r *http.Request) {
	localIP, _ := getLocalIP()
	io.WriteString(w, fmt.Sprintf("Hello %v from %v", r.Header.Get("X-Real-IP"), localIP))
}

func main() {
	http.HandleFunc("/", hello)
	http.ListenAndServe(":8000", nil)
}

func getLocalIP() (string, error) {
	eth0, err := net.InterfaceByName("eth0")
	if err != nil {
		return "", err
	}
	addrs, err := eth0.Addrs()
	if err != nil {
		return "", err
	}
	return addrs[0].String(), nil
}
